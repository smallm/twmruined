/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_GRAM_H_INCLUDED
# define YY_YY_GRAM_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    LB = 258,
    RB = 259,
    LP = 260,
    RP = 261,
    MENUS = 262,
    MENU = 263,
    BUTTON = 264,
    DEFAULT_FUNCTION = 265,
    PLUS = 266,
    MINUS = 267,
    ALL = 268,
    OR = 269,
    CURSORS = 270,
    ICONS = 271,
    COLOR = 272,
    SAVECOLOR = 273,
    MONOCHROME = 274,
    FUNCTION = 275,
    WINDOW_FUNCTION = 276,
    ZOOM = 277,
    MAKE_TITLE = 278,
    GRAYSCALE = 279,
    NO_TITLE = 280,
    AUTO_RAISE = 281,
    ICON_REGION = 282,
    META = 283,
    SHIFT = 284,
    LOCK = 285,
    CONTROL = 286,
    WINDOW = 287,
    TITLE = 288,
    ICON = 289,
    ROOT = 290,
    FRAME = 291,
    COLON = 292,
    EQUALS = 293,
    START_ICONIFIED = 294,
    MOVE = 295,
    RESIZE = 296,
    WAIT = 297,
    SELECT = 298,
    KILL = 299,
    LEFT_TITLEBUTTON = 300,
    RIGHT_TITLEBUTTON = 301,
    NUMBER = 302,
    KEYWORD = 303,
    NKEYWORD = 304,
    CKEYWORD = 305,
    CLKEYWORD = 306,
    FKEYWORD = 307,
    FSKEYWORD = 308,
    SKEYWORD = 309,
    DKEYWORD = 310,
    JKEYWORD = 311,
    WINDOW_RING = 312,
    WARP_CURSOR = 313,
    ERRORTOKEN = 314,
    NO_STACKMODE = 315,
    STRING = 316
  };
#endif
/* Tokens.  */
#define LB 258
#define RB 259
#define LP 260
#define RP 261
#define MENUS 262
#define MENU 263
#define BUTTON 264
#define DEFAULT_FUNCTION 265
#define PLUS 266
#define MINUS 267
#define ALL 268
#define OR 269
#define CURSORS 270
#define ICONS 271
#define COLOR 272
#define SAVECOLOR 273
#define MONOCHROME 274
#define FUNCTION 275
#define WINDOW_FUNCTION 276
#define ZOOM 277
#define MAKE_TITLE 278
#define GRAYSCALE 279
#define NO_TITLE 280
#define AUTO_RAISE 281
#define ICON_REGION 282
#define META 283
#define SHIFT 284
#define LOCK 285
#define CONTROL 286
#define WINDOW 287
#define TITLE 288
#define ICON 289
#define ROOT 290
#define FRAME 291
#define COLON 292
#define EQUALS 293
#define START_ICONIFIED 294
#define MOVE 295
#define RESIZE 296
#define WAIT 297
#define SELECT 298
#define KILL 299
#define LEFT_TITLEBUTTON 300
#define RIGHT_TITLEBUTTON 301
#define NUMBER 302
#define KEYWORD 303
#define NKEYWORD 304
#define CKEYWORD 305
#define CLKEYWORD 306
#define FKEYWORD 307
#define FSKEYWORD 308
#define SKEYWORD 309
#define DKEYWORD 310
#define JKEYWORD 311
#define WINDOW_RING 312
#define WARP_CURSOR 313
#define ERRORTOKEN 314
#define NO_STACKMODE 315
#define STRING 316

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 106 "gram.y" /* yacc.c:1909  */

    int num;
    char *ptr;

#line 181 "gram.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_GRAM_H_INCLUDED  */
