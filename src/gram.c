/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 67 "gram.y" /* yacc.c:339  */

#include <stdio.h>
#include <ctype.h>
#include "twm.h"
#include "menus.h"
#include "list.h"
#include "util.h"
#include "screen.h"
#include "parse.h"
#include "add_window.h"
#include <X11/Xos.h>
#include <X11/Xmu/CharSet.h>

static char *Action = "";
static char *Name = "";
static MenuRoot	*root, *pull = NULL;

static MenuRoot *GetRoot ( const char *name, const char *fore, const char *back );
static void GotButton ( int butt, int func );
static void GotKey ( char *key, int func );
static void GotTitleButton ( char *bitmapname, int func, Bool rightside );
static Bool CheckWarpScreenArg ( char *s );
static Bool CheckWarpRingArg ( char *s );
static Bool CheckColormapArg ( char *s );
static void RemoveDQuote ( char *str );

static char *ptr;
static name_list **list;
static int cont = 0;
static int color;
int mods = 0;
unsigned int mods_used = (ShiftMask | ControlMask | Mod1Mask);

extern int yylineno;
static void yyerror ( const char *s );


#line 104 "gram.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_YY_GRAM_H_INCLUDED
# define YY_YY_GRAM_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    LB = 258,
    RB = 259,
    LP = 260,
    RP = 261,
    MENUS = 262,
    MENU = 263,
    BUTTON = 264,
    DEFAULT_FUNCTION = 265,
    PLUS = 266,
    MINUS = 267,
    ALL = 268,
    OR = 269,
    CURSORS = 270,
    ICONS = 271,
    COLOR = 272,
    SAVECOLOR = 273,
    MONOCHROME = 274,
    FUNCTION = 275,
    WINDOW_FUNCTION = 276,
    ZOOM = 277,
    MAKE_TITLE = 278,
    GRAYSCALE = 279,
    NO_TITLE = 280,
    AUTO_RAISE = 281,
    ICON_REGION = 282,
    META = 283,
    SHIFT = 284,
    LOCK = 285,
    CONTROL = 286,
    WINDOW = 287,
    TITLE = 288,
    ICON = 289,
    ROOT = 290,
    FRAME = 291,
    COLON = 292,
    EQUALS = 293,
    START_ICONIFIED = 294,
    MOVE = 295,
    RESIZE = 296,
    WAIT = 297,
    SELECT = 298,
    KILL = 299,
    LEFT_TITLEBUTTON = 300,
    RIGHT_TITLEBUTTON = 301,
    NUMBER = 302,
    KEYWORD = 303,
    NKEYWORD = 304,
    CKEYWORD = 305,
    CLKEYWORD = 306,
    FKEYWORD = 307,
    FSKEYWORD = 308,
    SKEYWORD = 309,
    DKEYWORD = 310,
    JKEYWORD = 311,
    WINDOW_RING = 312,
    WARP_CURSOR = 313,
    ERRORTOKEN = 314,
    NO_STACKMODE = 315,
    STRING = 316
  };
#endif
/* Tokens.  */
#define LB 258
#define RB 259
#define LP 260
#define RP 261
#define MENUS 262
#define MENU 263
#define BUTTON 264
#define DEFAULT_FUNCTION 265
#define PLUS 266
#define MINUS 267
#define ALL 268
#define OR 269
#define CURSORS 270
#define ICONS 271
#define COLOR 272
#define SAVECOLOR 273
#define MONOCHROME 274
#define FUNCTION 275
#define WINDOW_FUNCTION 276
#define ZOOM 277
#define MAKE_TITLE 278
#define GRAYSCALE 279
#define NO_TITLE 280
#define AUTO_RAISE 281
#define ICON_REGION 282
#define META 283
#define SHIFT 284
#define LOCK 285
#define CONTROL 286
#define WINDOW 287
#define TITLE 288
#define ICON 289
#define ROOT 290
#define FRAME 291
#define COLON 292
#define EQUALS 293
#define START_ICONIFIED 294
#define MOVE 295
#define RESIZE 296
#define WAIT 297
#define SELECT 298
#define KILL 299
#define LEFT_TITLEBUTTON 300
#define RIGHT_TITLEBUTTON 301
#define NUMBER 302
#define KEYWORD 303
#define NKEYWORD 304
#define CKEYWORD 305
#define CLKEYWORD 306
#define FKEYWORD 307
#define FSKEYWORD 308
#define SKEYWORD 309
#define DKEYWORD 310
#define JKEYWORD 311
#define WINDOW_RING 312
#define WARP_CURSOR 313
#define ERRORTOKEN 314
#define NO_STACKMODE 315
#define STRING 316

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 106 "gram.y" /* yacc.c:355  */

    int num;
    char *ptr;

#line 271 "gram.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_GRAM_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 288 "gram.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  3
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   214

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  62
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  58
/* YYNRULES -- Number of rules.  */
#define YYNRULES  144
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  213

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   316

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   133,   133,   136,   137,   140,   141,   142,   143,   144,
     150,   152,   153,   156,   159,   163,   179,   180,   181,   181,
     183,   185,   185,   187,   189,   189,   191,   191,   193,   193,
     195,   195,   198,   198,   200,   200,   202,   202,   204,   204,
     206,   206,   208,   210,   210,   212,   228,   236,   236,   238,
     240,   240,   245,   255,   265,   277,   280,   283,   284,   287,
     288,   289,   290,   291,   301,   304,   305,   308,   309,   310,
     311,   312,   313,   314,   317,   318,   321,   322,   323,   324,
     325,   326,   327,   328,   331,   334,   335,   338,   340,   342,
     344,   346,   348,   350,   352,   354,   356,   358,   360,   362,
     364,   366,   368,   370,   372,   374,   376,   380,   384,   385,
     388,   397,   397,   408,   419,   422,   423,   426,   427,   430,
     433,   434,   437,   442,   445,   446,   449,   454,   457,   458,
     461,   464,   467,   468,   471,   477,   480,   481,   484,   489,
     497,   498,   544,   556,   561
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "LB", "RB", "LP", "RP", "MENUS", "MENU",
  "BUTTON", "DEFAULT_FUNCTION", "PLUS", "MINUS", "ALL", "OR", "CURSORS",
  "ICONS", "COLOR", "SAVECOLOR", "MONOCHROME", "FUNCTION",
  "WINDOW_FUNCTION", "ZOOM", "MAKE_TITLE", "GRAYSCALE", "NO_TITLE",
  "AUTO_RAISE", "ICON_REGION", "META", "SHIFT", "LOCK", "CONTROL",
  "WINDOW", "TITLE", "ICON", "ROOT", "FRAME", "COLON", "EQUALS",
  "START_ICONIFIED", "MOVE", "RESIZE", "WAIT", "SELECT", "KILL",
  "LEFT_TITLEBUTTON", "RIGHT_TITLEBUTTON", "NUMBER", "KEYWORD", "NKEYWORD",
  "CKEYWORD", "CLKEYWORD", "FKEYWORD", "FSKEYWORD", "SKEYWORD", "DKEYWORD",
  "JKEYWORD", "WINDOW_RING", "WARP_CURSOR", "ERRORTOKEN", "NO_STACKMODE",
  "STRING", "$accept", "twmrc", "stmts", "stmt", "$@1", "$@2", "$@3",
  "$@4", "$@5", "$@6", "$@7", "$@8", "$@9", "$@10", "$@11", "$@12", "$@13",
  "$@14", "noarg", "sarg", "narg", "full", "fullkey", "keys", "key",
  "contexts", "context", "contextkeys", "contextkey", "cursor_list",
  "cursor_entries", "cursor_entry", "color_list", "color_entries",
  "color_entry", "$@15", "save_color_list", "s_color_entries",
  "s_color_entry", "win_color_list", "win_color_entries",
  "win_color_entry", "win_list", "win_entries", "win_entry", "icon_list",
  "icon_entries", "icon_entry", "function", "function_entries",
  "function_entry", "menu", "menu_entries", "menu_entry", "action",
  "button", "string", "number", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316
};
# endif

#define YYPACT_NINF -109

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-109)))

#define YYTABLE_NINF -112

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
    -109,    12,    23,  -109,  -109,   -46,   -29,   -27,    18,  -109,
    -109,    27,  -109,   -46,   -27,   -29,  -109,  -109,    31,  -109,
    -109,   -46,   -46,  -109,   -29,   -46,  -109,    34,    48,  -109,
    -109,  -109,  -109,  -109,   -25,    14,    50,  -109,  -109,  -109,
     -46,  -109,  -109,  -109,    53,    54,  -109,  -109,    54,  -109,
    -109,  -109,    67,    54,    67,    67,    67,    36,    40,  -109,
    -109,    67,    67,    67,  -109,  -109,  -109,  -109,  -109,  -109,
     -46,    76,  -109,   113,  -109,  -109,  -109,  -109,    -1,  -109,
      79,  -109,  -109,  -109,  -109,  -109,  -109,   -27,   -27,  -109,
    -109,  -109,   154,   165,    49,  -109,  -109,  -109,   -46,   -46,
     -46,   -46,   -46,   -46,   -46,   -46,   -46,   -46,  -109,     0,
      63,  -109,  -109,  -109,  -109,  -109,  -109,     2,  -109,  -109,
    -109,   -29,  -109,  -109,  -109,  -109,  -109,  -109,   -46,     3,
     -46,   -46,   -46,   -46,   -46,   -46,   -46,   -46,   -46,   -46,
    -109,  -109,   -46,  -109,   -46,   -46,  -109,     1,  -109,  -109,
    -109,  -109,   138,   129,    81,  -109,  -109,     6,  -109,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
      82,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,   -27,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
     -27,  -109,  -109,  -109,   -46,  -109,    85,  -109,  -109,    76,
      52,  -109,  -109,  -109,   -46,     5,    84,  -109,  -109,   -46,
     -27,  -109,  -109
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       3,     0,     0,     1,     5,     0,     0,     0,     0,    36,
      38,     0,    43,     0,     0,    10,    24,    40,    23,    28,
      26,     0,     0,    52,     0,     0,    50,    49,    20,   143,
       4,     6,     7,     8,     0,     0,    32,   144,   142,   140,
       0,    45,    85,    11,     0,     0,   115,    42,     0,    34,
      46,     9,     0,     0,     0,     0,     0,     0,     0,    54,
      53,     0,     0,     0,    57,    17,    15,    14,    57,    16,
       0,     0,   141,     0,   128,    37,   108,    39,     0,    44,
       0,   124,    25,    41,    22,    29,    27,     0,     0,    51,
      48,    19,     0,     0,     0,   136,    33,    84,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    86,     0,
       0,   114,   118,   116,   117,   132,    35,     0,    12,    13,
      64,    59,    60,    61,    62,    65,    58,    74,     0,     0,
     102,    94,    90,    92,    88,    96,    98,   100,   104,   106,
     127,   129,     0,   107,     0,     0,   109,     0,   123,   125,
     126,    63,     0,     0,     0,   135,   137,     0,   101,    93,
      89,    91,    87,    95,    97,    99,   103,   105,   130,   113,
     110,   131,   133,   134,    72,    73,    67,    68,    69,    70,
      71,     0,    66,    81,    82,    76,    77,    78,    79,    80,
       0,    75,    83,    30,     0,   138,     0,    55,    56,     0,
       0,   120,   112,    31,     0,     0,     0,   119,   121,     0,
       0,   122,   139
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,    24,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,   -34,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,   151,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -108,  -109,  -109,   -12,  -109,    -5,   -14
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     2,    30,    63,    54,    52,    56,    55,   199,
      71,    80,    44,    45,    53,    48,    62,    61,    31,    32,
      33,    65,    69,    92,   126,   152,   182,   153,   191,    43,
      73,   108,    77,   110,   146,   196,    47,    78,   113,   202,
     205,   208,    82,   117,   149,    75,   109,   141,   116,   147,
     172,    96,   129,   156,    41,    34,    35,    38
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      36,    51,    50,   111,   140,   171,   148,   155,    49,   207,
      59,   194,     3,    64,    79,    29,    57,    58,    37,    83,
      60,    42,    66,    -2,     4,    39,    40,    39,    40,    67,
      46,     5,     6,     7,   -21,    72,    29,   -47,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
     112,   -18,    68,    39,    40,    70,    74,    76,    39,    40,
      29,    29,    20,    29,    29,    94,    29,   143,    21,    22,
      81,    23,    24,   114,    87,   118,   119,    25,    88,    95,
      26,    27,   115,    28,    29,  -111,   128,   193,   201,   204,
     210,   203,    93,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   139,     0,   142,     0,     0,   151,     0,     0,
       0,     0,   150,   144,   145,     0,     0,    97,     0,     0,
       0,    98,    99,   154,   157,   158,   159,   160,   161,   162,
     163,   164,   165,   166,   167,   173,     0,   168,     0,   169,
     170,     0,   183,   184,     0,   195,   100,   101,   192,   102,
       0,   174,   175,   103,   104,   105,   106,   107,     0,     0,
       0,   185,   186,   187,   188,   189,   190,     0,   120,   197,
     176,   177,   178,   179,   180,   181,     0,     0,   198,   120,
       0,     0,   121,   122,   123,   124,     0,     0,     0,   200,
      29,   125,     0,   121,   122,   123,   124,     0,   212,   206,
     209,     0,   127,     0,   211,    84,    85,    86,     0,     0,
       0,     0,    89,    90,    91
};

static const yytype_int16 yycheck[] =
{
       5,    15,    14,     4,     4,     4,     4,     4,    13,     4,
      24,     5,     0,    38,    48,    61,    21,    22,    47,    53,
      25,     3,    34,     0,     1,    52,    53,    52,    53,    34,
       3,     8,     9,    10,     3,    40,    61,     3,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      51,     3,    38,    52,    53,     5,     3,     3,    52,    53,
      61,    61,    39,    61,    61,    70,    61,     4,    45,    46,
       3,    48,    49,    78,    38,    87,    88,    54,    38,     3,
      57,    58,     3,    60,    61,     3,    37,     6,     3,    37,
       6,   199,    68,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,    -1,   109,    -1,    -1,   121,    -1,    -1,
      -1,    -1,   117,    50,    51,    -1,    -1,     4,    -1,    -1,
      -1,     8,     9,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   147,    -1,   142,    -1,   144,
     145,    -1,    13,    14,    -1,   157,    33,    34,   153,    36,
      -1,    13,    14,    40,    41,    42,    43,    44,    -1,    -1,
      -1,    32,    33,    34,    35,    36,    37,    -1,    14,   181,
      32,    33,    34,    35,    36,    37,    -1,    -1,   190,    14,
      -1,    -1,    28,    29,    30,    31,    -1,    -1,    -1,   194,
      61,    37,    -1,    28,    29,    30,    31,    -1,   210,   204,
     205,    -1,    37,    -1,   209,    54,    55,    56,    -1,    -1,
      -1,    -1,    61,    62,    63
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    63,    64,     0,     1,     8,     9,    10,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      39,    45,    46,    48,    49,    54,    57,    58,    60,    61,
      65,    80,    81,    82,   117,   118,   118,    47,   119,    52,
      53,   116,     3,    91,    74,    75,     3,    98,    77,   118,
     116,   119,    68,    76,    67,    70,    69,   118,   118,   119,
     118,    79,    78,    66,    38,    83,   116,   118,    38,    84,
       5,    72,   118,    92,     3,   107,     3,    94,    99,    94,
      73,     3,   104,    94,   104,   104,   104,    38,    38,   104,
     104,   104,    85,    85,   118,     3,   113,     4,     8,     9,
      33,    34,    36,    40,    41,    42,    43,    44,    93,   108,
      95,     4,    51,   100,   118,     3,   110,   105,   116,   116,
      14,    28,    29,    30,    31,    37,    86,    37,    37,   114,
     118,   118,   118,   118,   118,   118,   118,   118,   118,   118,
       4,   109,   118,     4,    50,    51,    96,   111,     4,   106,
     118,   119,    87,    89,   118,     4,   115,   118,   118,   118,
     118,   118,   118,   118,   118,   118,   118,   118,   118,   118,
     118,     4,   112,   116,    13,    14,    32,    33,    34,    35,
      36,    37,    88,    13,    14,    32,    33,    34,    35,    36,
      37,    90,   118,     6,     5,   116,    97,   116,   116,    71,
     118,     3,   101,   113,    37,   102,   118,     4,   103,   118,
       6,   118,   116
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    62,    63,    64,    64,    65,    65,    65,    65,    65,
      65,    65,    65,    65,    65,    65,    65,    65,    66,    65,
      65,    67,    65,    65,    68,    65,    69,    65,    70,    65,
      71,    65,    72,    65,    73,    65,    74,    65,    75,    65,
      76,    65,    65,    77,    65,    65,    65,    78,    65,    65,
      79,    65,    80,    81,    82,    83,    84,    85,    85,    86,
      86,    86,    86,    86,    86,    87,    87,    88,    88,    88,
      88,    88,    88,    88,    89,    89,    90,    90,    90,    90,
      90,    90,    90,    90,    91,    92,    92,    93,    93,    93,
      93,    93,    93,    93,    93,    93,    93,    93,    93,    93,
      93,    93,    93,    93,    93,    93,    93,    94,    95,    95,
      96,    97,    96,    96,    98,    99,    99,   100,   100,   101,
     102,   102,   103,   104,   105,   105,   106,   107,   108,   108,
     109,   110,   111,   111,   112,   113,   114,   114,   115,   115,
     116,   116,   117,   118,   119
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     0,     2,     1,     1,     1,     1,     2,
       1,     2,     4,     4,     2,     2,     2,     2,     0,     3,
       1,     0,     3,     1,     0,     3,     0,     3,     0,     3,
       0,     9,     0,     4,     0,     4,     0,     3,     0,     3,
       0,     3,     2,     0,     3,     2,     2,     0,     3,     1,
       0,     3,     1,     2,     2,     6,     6,     0,     2,     1,
       1,     1,     1,     2,     1,     0,     2,     1,     1,     1,
       1,     1,     1,     1,     0,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     3,     0,     2,     3,     2,     3,
       2,     3,     2,     3,     2,     3,     2,     3,     2,     3,
       2,     3,     2,     3,     2,     3,     2,     3,     0,     2,
       2,     0,     4,     2,     3,     0,     2,     1,     1,     3,
       0,     2,     2,     3,     0,     2,     1,     3,     0,     2,
       2,     3,     0,     2,     1,     3,     0,     2,     2,     7,
       1,     2,     2,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 9:
#line 144 "gram.y" /* yacc.c:1646  */
    { if (Scr->FirstTime)
					  {
						Scr->DoZoom = TRUE;
						Scr->ZoomCount = (yyvsp[0].num);
					  }
					}
#line 1546 "gram.c" /* yacc.c:1646  */
    break;

  case 10:
#line 150 "gram.y" /* yacc.c:1646  */
    { if (Scr->FirstTime)
						Scr->DoZoom = TRUE; }
#line 1553 "gram.c" /* yacc.c:1646  */
    break;

  case 11:
#line 152 "gram.y" /* yacc.c:1646  */
    {}
#line 1559 "gram.c" /* yacc.c:1646  */
    break;

  case 12:
#line 153 "gram.y" /* yacc.c:1646  */
    {
					  GotTitleButton ((yyvsp[-2].ptr), (yyvsp[0].num), False);
					}
#line 1567 "gram.c" /* yacc.c:1646  */
    break;

  case 13:
#line 156 "gram.y" /* yacc.c:1646  */
    {
					  GotTitleButton ((yyvsp[-2].ptr), (yyvsp[0].num), True);
					}
#line 1575 "gram.c" /* yacc.c:1646  */
    break;

  case 14:
#line 159 "gram.y" /* yacc.c:1646  */
    { root = GetRoot((yyvsp[0].ptr), NULLSTR, NULLSTR);
					  Scr->Mouse[(yyvsp[-1].num)][C_ROOT][0].func = F_MENU;
					  Scr->Mouse[(yyvsp[-1].num)][C_ROOT][0].menu = root;
					}
#line 1584 "gram.c" /* yacc.c:1646  */
    break;

  case 15:
#line 163 "gram.y" /* yacc.c:1646  */
    { Scr->Mouse[(yyvsp[-1].num)][C_ROOT][0].func = (yyvsp[0].num);
					  if ((yyvsp[0].num) == F_MENU)
					  {
					    pull->prev = NULL;
					    Scr->Mouse[(yyvsp[-1].num)][C_ROOT][0].menu = pull;
					  }
					  else
					  {
					    root = GetRoot(TWM_ROOT,NULLSTR,NULLSTR);
					    Scr->Mouse[(yyvsp[-1].num)][C_ROOT][0].item =
						AddToMenu(root,"x",Action,
							  NULL,(yyvsp[0].num),NULLSTR,NULLSTR);
					  }
					  Action = "";
					  pull = NULL;
					}
#line 1605 "gram.c" /* yacc.c:1646  */
    break;

  case 16:
#line 179 "gram.y" /* yacc.c:1646  */
    { GotKey((yyvsp[-1].ptr), (yyvsp[0].num)); }
#line 1611 "gram.c" /* yacc.c:1646  */
    break;

  case 17:
#line 180 "gram.y" /* yacc.c:1646  */
    { GotButton((yyvsp[-1].num), (yyvsp[0].num)); }
#line 1617 "gram.c" /* yacc.c:1646  */
    break;

  case 18:
#line 181 "gram.y" /* yacc.c:1646  */
    { list = &Scr->NoStackModeL; }
#line 1623 "gram.c" /* yacc.c:1646  */
    break;

  case 20:
#line 183 "gram.y" /* yacc.c:1646  */
    { if (Scr->FirstTime)
						Scr->StackMode = FALSE; }
#line 1630 "gram.c" /* yacc.c:1646  */
    break;

  case 21:
#line 185 "gram.y" /* yacc.c:1646  */
    { list = &Scr->NoTitle; }
#line 1636 "gram.c" /* yacc.c:1646  */
    break;

  case 23:
#line 187 "gram.y" /* yacc.c:1646  */
    { if (Scr->FirstTime)
						Scr->NoTitlebar = TRUE; }
#line 1643 "gram.c" /* yacc.c:1646  */
    break;

  case 24:
#line 189 "gram.y" /* yacc.c:1646  */
    { list = &Scr->MakeTitle; }
#line 1649 "gram.c" /* yacc.c:1646  */
    break;

  case 26:
#line 191 "gram.y" /* yacc.c:1646  */
    { list = &Scr->StartIconified; }
#line 1655 "gram.c" /* yacc.c:1646  */
    break;

  case 28:
#line 193 "gram.y" /* yacc.c:1646  */
    { list = &Scr->AutoRaise; }
#line 1661 "gram.c" /* yacc.c:1646  */
    break;

  case 30:
#line 195 "gram.y" /* yacc.c:1646  */
    {
					root = GetRoot((yyvsp[-5].ptr), (yyvsp[-3].ptr), (yyvsp[-1].ptr)); }
#line 1668 "gram.c" /* yacc.c:1646  */
    break;

  case 31:
#line 197 "gram.y" /* yacc.c:1646  */
    { root->real_menu = TRUE;}
#line 1674 "gram.c" /* yacc.c:1646  */
    break;

  case 32:
#line 198 "gram.y" /* yacc.c:1646  */
    { root = GetRoot((yyvsp[0].ptr), NULLSTR, NULLSTR); }
#line 1680 "gram.c" /* yacc.c:1646  */
    break;

  case 33:
#line 199 "gram.y" /* yacc.c:1646  */
    { root->real_menu = TRUE; }
#line 1686 "gram.c" /* yacc.c:1646  */
    break;

  case 34:
#line 200 "gram.y" /* yacc.c:1646  */
    { root = GetRoot((yyvsp[0].ptr), NULLSTR, NULLSTR); }
#line 1692 "gram.c" /* yacc.c:1646  */
    break;

  case 36:
#line 202 "gram.y" /* yacc.c:1646  */
    { list = &Scr->IconNames; }
#line 1698 "gram.c" /* yacc.c:1646  */
    break;

  case 38:
#line 204 "gram.y" /* yacc.c:1646  */
    { color = COLOR; }
#line 1704 "gram.c" /* yacc.c:1646  */
    break;

  case 40:
#line 206 "gram.y" /* yacc.c:1646  */
    { color = GRAYSCALE; }
#line 1710 "gram.c" /* yacc.c:1646  */
    break;

  case 43:
#line 210 "gram.y" /* yacc.c:1646  */
    { color = MONOCHROME; }
#line 1716 "gram.c" /* yacc.c:1646  */
    break;

  case 45:
#line 212 "gram.y" /* yacc.c:1646  */
    { Scr->DefaultFunction.func = (yyvsp[0].num);
					  if ((yyvsp[0].num) == F_MENU)
					  {
					    pull->prev = NULL;
					    Scr->DefaultFunction.menu = pull;
					  }
					  else
					  {
					    root = GetRoot(TWM_ROOT,NULLSTR,NULLSTR);
					    Scr->DefaultFunction.item =
						AddToMenu(root,"x",Action,
							  NULL,(yyvsp[0].num), NULLSTR, NULLSTR);
					  }
					  Action = "";
					  pull = NULL;
					}
#line 1737 "gram.c" /* yacc.c:1646  */
    break;

  case 46:
#line 228 "gram.y" /* yacc.c:1646  */
    { Scr->WindowFunction.func = (yyvsp[0].num);
					   root = GetRoot(TWM_ROOT,NULLSTR,NULLSTR);
					   Scr->WindowFunction.item =
						AddToMenu(root,"x",Action,
							  NULL,(yyvsp[0].num), NULLSTR, NULLSTR);
					   Action = "";
					   pull = NULL;
					}
#line 1750 "gram.c" /* yacc.c:1646  */
    break;

  case 47:
#line 236 "gram.y" /* yacc.c:1646  */
    { list = &Scr->WarpCursorL; }
#line 1756 "gram.c" /* yacc.c:1646  */
    break;

  case 49:
#line 238 "gram.y" /* yacc.c:1646  */
    { if (Scr->FirstTime)
					    Scr->WarpCursor = TRUE; }
#line 1763 "gram.c" /* yacc.c:1646  */
    break;

  case 50:
#line 240 "gram.y" /* yacc.c:1646  */
    { list = &Scr->WindowRingL; }
#line 1769 "gram.c" /* yacc.c:1646  */
    break;

  case 52:
#line 245 "gram.y" /* yacc.c:1646  */
    { if (!do_single_keyword ((yyvsp[0].num))) {
					    twmrc_error_prefix();
					    fprintf (stderr,
					"unknown singleton keyword %d\n",
						     (yyvsp[0].num));
					    ParseError = 1;
					  }
					}
#line 1782 "gram.c" /* yacc.c:1646  */
    break;

  case 53:
#line 255 "gram.y" /* yacc.c:1646  */
    { if (!do_string_keyword ((yyvsp[-1].num), (yyvsp[0].ptr))) {
					    twmrc_error_prefix();
					    fprintf (stderr,
				"unknown string keyword %d (value \"%s\")\n",
						     (yyvsp[-1].num), (yyvsp[0].ptr));
					    ParseError = 1;
					  }
					}
#line 1795 "gram.c" /* yacc.c:1646  */
    break;

  case 54:
#line 265 "gram.y" /* yacc.c:1646  */
    { if (!do_number_keyword ((yyvsp[-1].num), (yyvsp[0].num))) {
					    twmrc_error_prefix();
					    fprintf (stderr,
				"unknown numeric keyword %d (value %d)\n",
						     (yyvsp[-1].num), (yyvsp[0].num));
					    ParseError = 1;
					  }
					}
#line 1808 "gram.c" /* yacc.c:1646  */
    break;

  case 55:
#line 277 "gram.y" /* yacc.c:1646  */
    { (yyval.num) = (yyvsp[0].num); }
#line 1814 "gram.c" /* yacc.c:1646  */
    break;

  case 56:
#line 280 "gram.y" /* yacc.c:1646  */
    { (yyval.num) = (yyvsp[0].num); }
#line 1820 "gram.c" /* yacc.c:1646  */
    break;

  case 59:
#line 287 "gram.y" /* yacc.c:1646  */
    { mods |= Mod1Mask; }
#line 1826 "gram.c" /* yacc.c:1646  */
    break;

  case 60:
#line 288 "gram.y" /* yacc.c:1646  */
    { mods |= ShiftMask; }
#line 1832 "gram.c" /* yacc.c:1646  */
    break;

  case 61:
#line 289 "gram.y" /* yacc.c:1646  */
    { mods |= LockMask; }
#line 1838 "gram.c" /* yacc.c:1646  */
    break;

  case 62:
#line 290 "gram.y" /* yacc.c:1646  */
    { mods |= ControlMask; }
#line 1844 "gram.c" /* yacc.c:1646  */
    break;

  case 63:
#line 291 "gram.y" /* yacc.c:1646  */
    { if ((yyvsp[0].num) < 1 || (yyvsp[0].num) > 5) {
					     twmrc_error_prefix();
					     fprintf (stderr,
				"bad modifier number (%d), must be 1-5\n",
						      (yyvsp[0].num));
					     ParseError = 1;
					  } else {
					     mods |= (Mod1Mask << ((yyvsp[0].num) - 1));
					  }
					}
#line 1859 "gram.c" /* yacc.c:1646  */
    break;

  case 64:
#line 301 "gram.y" /* yacc.c:1646  */
    { }
#line 1865 "gram.c" /* yacc.c:1646  */
    break;

  case 67:
#line 308 "gram.y" /* yacc.c:1646  */
    { cont |= C_WINDOW_BIT; }
#line 1871 "gram.c" /* yacc.c:1646  */
    break;

  case 68:
#line 309 "gram.y" /* yacc.c:1646  */
    { cont |= C_TITLE_BIT; }
#line 1877 "gram.c" /* yacc.c:1646  */
    break;

  case 69:
#line 310 "gram.y" /* yacc.c:1646  */
    { cont |= C_ICON_BIT; }
#line 1883 "gram.c" /* yacc.c:1646  */
    break;

  case 70:
#line 311 "gram.y" /* yacc.c:1646  */
    { cont |= C_ROOT_BIT; }
#line 1889 "gram.c" /* yacc.c:1646  */
    break;

  case 71:
#line 312 "gram.y" /* yacc.c:1646  */
    { cont |= C_FRAME_BIT; }
#line 1895 "gram.c" /* yacc.c:1646  */
    break;

  case 72:
#line 313 "gram.y" /* yacc.c:1646  */
    { cont |= C_ALL_BITS; }
#line 1901 "gram.c" /* yacc.c:1646  */
    break;

  case 73:
#line 314 "gram.y" /* yacc.c:1646  */
    {  }
#line 1907 "gram.c" /* yacc.c:1646  */
    break;

  case 76:
#line 321 "gram.y" /* yacc.c:1646  */
    { cont |= C_WINDOW_BIT; }
#line 1913 "gram.c" /* yacc.c:1646  */
    break;

  case 77:
#line 322 "gram.y" /* yacc.c:1646  */
    { cont |= C_TITLE_BIT; }
#line 1919 "gram.c" /* yacc.c:1646  */
    break;

  case 78:
#line 323 "gram.y" /* yacc.c:1646  */
    { cont |= C_ICON_BIT; }
#line 1925 "gram.c" /* yacc.c:1646  */
    break;

  case 79:
#line 324 "gram.y" /* yacc.c:1646  */
    { cont |= C_ROOT_BIT; }
#line 1931 "gram.c" /* yacc.c:1646  */
    break;

  case 80:
#line 325 "gram.y" /* yacc.c:1646  */
    { cont |= C_FRAME_BIT; }
#line 1937 "gram.c" /* yacc.c:1646  */
    break;

  case 81:
#line 326 "gram.y" /* yacc.c:1646  */
    { cont |= C_ALL_BITS; }
#line 1943 "gram.c" /* yacc.c:1646  */
    break;

  case 82:
#line 327 "gram.y" /* yacc.c:1646  */
    { }
#line 1949 "gram.c" /* yacc.c:1646  */
    break;

  case 83:
#line 328 "gram.y" /* yacc.c:1646  */
    { Name = (yyvsp[0].ptr); cont |= C_NAME_BIT; }
#line 1955 "gram.c" /* yacc.c:1646  */
    break;

  case 87:
#line 338 "gram.y" /* yacc.c:1646  */
    {
			NewBitmapCursor(&Scr->FrameCursor, (yyvsp[-1].ptr), (yyvsp[0].ptr)); }
#line 1962 "gram.c" /* yacc.c:1646  */
    break;

  case 88:
#line 340 "gram.y" /* yacc.c:1646  */
    {
			NewFontCursor(&Scr->FrameCursor, (yyvsp[0].ptr)); }
#line 1969 "gram.c" /* yacc.c:1646  */
    break;

  case 89:
#line 342 "gram.y" /* yacc.c:1646  */
    {
			NewBitmapCursor(&Scr->TitleCursor, (yyvsp[-1].ptr), (yyvsp[0].ptr)); }
#line 1976 "gram.c" /* yacc.c:1646  */
    break;

  case 90:
#line 344 "gram.y" /* yacc.c:1646  */
    {
			NewFontCursor(&Scr->TitleCursor, (yyvsp[0].ptr)); }
#line 1983 "gram.c" /* yacc.c:1646  */
    break;

  case 91:
#line 346 "gram.y" /* yacc.c:1646  */
    {
			NewBitmapCursor(&Scr->IconCursor, (yyvsp[-1].ptr), (yyvsp[0].ptr)); }
#line 1990 "gram.c" /* yacc.c:1646  */
    break;

  case 92:
#line 348 "gram.y" /* yacc.c:1646  */
    {
			NewFontCursor(&Scr->IconCursor, (yyvsp[0].ptr)); }
#line 1997 "gram.c" /* yacc.c:1646  */
    break;

  case 93:
#line 350 "gram.y" /* yacc.c:1646  */
    {
			NewBitmapCursor(&Scr->ButtonCursor, (yyvsp[-1].ptr), (yyvsp[0].ptr)); }
#line 2004 "gram.c" /* yacc.c:1646  */
    break;

  case 94:
#line 352 "gram.y" /* yacc.c:1646  */
    {
			NewFontCursor(&Scr->ButtonCursor, (yyvsp[0].ptr)); }
#line 2011 "gram.c" /* yacc.c:1646  */
    break;

  case 95:
#line 354 "gram.y" /* yacc.c:1646  */
    {
			NewBitmapCursor(&Scr->MoveCursor, (yyvsp[-1].ptr), (yyvsp[0].ptr)); }
#line 2018 "gram.c" /* yacc.c:1646  */
    break;

  case 96:
#line 356 "gram.y" /* yacc.c:1646  */
    {
			NewFontCursor(&Scr->MoveCursor, (yyvsp[0].ptr)); }
#line 2025 "gram.c" /* yacc.c:1646  */
    break;

  case 97:
#line 358 "gram.y" /* yacc.c:1646  */
    {
			NewBitmapCursor(&Scr->ResizeCursor, (yyvsp[-1].ptr), (yyvsp[0].ptr)); }
#line 2032 "gram.c" /* yacc.c:1646  */
    break;

  case 98:
#line 360 "gram.y" /* yacc.c:1646  */
    {
			NewFontCursor(&Scr->ResizeCursor, (yyvsp[0].ptr)); }
#line 2039 "gram.c" /* yacc.c:1646  */
    break;

  case 99:
#line 362 "gram.y" /* yacc.c:1646  */
    {
			NewBitmapCursor(&Scr->WaitCursor, (yyvsp[-1].ptr), (yyvsp[0].ptr)); }
#line 2046 "gram.c" /* yacc.c:1646  */
    break;

  case 100:
#line 364 "gram.y" /* yacc.c:1646  */
    {
			NewFontCursor(&Scr->WaitCursor, (yyvsp[0].ptr)); }
#line 2053 "gram.c" /* yacc.c:1646  */
    break;

  case 101:
#line 366 "gram.y" /* yacc.c:1646  */
    {
			NewBitmapCursor(&Scr->MenuCursor, (yyvsp[-1].ptr), (yyvsp[0].ptr)); }
#line 2060 "gram.c" /* yacc.c:1646  */
    break;

  case 102:
#line 368 "gram.y" /* yacc.c:1646  */
    {
			NewFontCursor(&Scr->MenuCursor, (yyvsp[0].ptr)); }
#line 2067 "gram.c" /* yacc.c:1646  */
    break;

  case 103:
#line 370 "gram.y" /* yacc.c:1646  */
    {
			NewBitmapCursor(&Scr->SelectCursor, (yyvsp[-1].ptr), (yyvsp[0].ptr)); }
#line 2074 "gram.c" /* yacc.c:1646  */
    break;

  case 104:
#line 372 "gram.y" /* yacc.c:1646  */
    {
			NewFontCursor(&Scr->SelectCursor, (yyvsp[0].ptr)); }
#line 2081 "gram.c" /* yacc.c:1646  */
    break;

  case 105:
#line 374 "gram.y" /* yacc.c:1646  */
    {
			NewBitmapCursor(&Scr->DestroyCursor, (yyvsp[-1].ptr), (yyvsp[0].ptr)); }
#line 2088 "gram.c" /* yacc.c:1646  */
    break;

  case 106:
#line 376 "gram.y" /* yacc.c:1646  */
    {
			NewFontCursor(&Scr->DestroyCursor, (yyvsp[0].ptr)); }
#line 2095 "gram.c" /* yacc.c:1646  */
    break;

  case 110:
#line 388 "gram.y" /* yacc.c:1646  */
    { if (!do_colorlist_keyword ((yyvsp[-1].num), color,
								     (yyvsp[0].ptr))) {
					    twmrc_error_prefix();
					    fprintf (stderr,
			"unhandled list color keyword %d (string \"%s\")\n",
						     (yyvsp[-1].num), (yyvsp[0].ptr));
					    ParseError = 1;
					  }
					}
#line 2109 "gram.c" /* yacc.c:1646  */
    break;

  case 111:
#line 397 "gram.y" /* yacc.c:1646  */
    { list = do_colorlist_keyword((yyvsp[-1].num), color,
								      (yyvsp[0].ptr));
					  if (!list) {
					    twmrc_error_prefix();
					    fprintf (stderr,
			"unhandled color list keyword %d (string \"%s\")\n",
						     (yyvsp[-1].num), (yyvsp[0].ptr));
					    ParseError = 1;
					  }
					}
#line 2124 "gram.c" /* yacc.c:1646  */
    break;

  case 112:
#line 407 "gram.y" /* yacc.c:1646  */
    { /* No action */; }
#line 2130 "gram.c" /* yacc.c:1646  */
    break;

  case 113:
#line 408 "gram.y" /* yacc.c:1646  */
    { if (!do_color_keyword ((yyvsp[-1].num), color,
								 (yyvsp[0].ptr))) {
					    twmrc_error_prefix();
					    fprintf (stderr,
			"unhandled color keyword %d (string \"%s\")\n",
						     (yyvsp[-1].num), (yyvsp[0].ptr));
					    ParseError = 1;
					  }
					}
#line 2144 "gram.c" /* yacc.c:1646  */
    break;

  case 117:
#line 426 "gram.y" /* yacc.c:1646  */
    { do_string_savecolor(color, (yyvsp[0].ptr)); }
#line 2150 "gram.c" /* yacc.c:1646  */
    break;

  case 118:
#line 427 "gram.y" /* yacc.c:1646  */
    { do_var_savecolor((yyvsp[0].num)); }
#line 2156 "gram.c" /* yacc.c:1646  */
    break;

  case 122:
#line 437 "gram.y" /* yacc.c:1646  */
    { if (Scr->FirstTime &&
					      color == Scr->Monochrome)
					    AddToList(list, (yyvsp[-1].ptr), (yyvsp[0].ptr)); }
#line 2164 "gram.c" /* yacc.c:1646  */
    break;

  case 126:
#line 449 "gram.y" /* yacc.c:1646  */
    { if (Scr->FirstTime)
					    AddToList(list, (yyvsp[0].ptr), 0);
					}
#line 2172 "gram.c" /* yacc.c:1646  */
    break;

  case 130:
#line 461 "gram.y" /* yacc.c:1646  */
    { if (Scr->FirstTime) AddToList(list, (yyvsp[-1].ptr), (yyvsp[0].ptr)); }
#line 2178 "gram.c" /* yacc.c:1646  */
    break;

  case 134:
#line 471 "gram.y" /* yacc.c:1646  */
    { AddToMenu(root, "", Action, NULL, (yyvsp[0].num),
						NULLSTR, NULLSTR);
					  Action = "";
					}
#line 2187 "gram.c" /* yacc.c:1646  */
    break;

  case 138:
#line 484 "gram.y" /* yacc.c:1646  */
    { AddToMenu(root, (yyvsp[-1].ptr), Action, pull, (yyvsp[0].num),
						NULLSTR, NULLSTR);
					  Action = "";
					  pull = NULL;
					}
#line 2197 "gram.c" /* yacc.c:1646  */
    break;

  case 139:
#line 489 "gram.y" /* yacc.c:1646  */
    {
					  AddToMenu(root, (yyvsp[-6].ptr), Action, pull, (yyvsp[0].num),
						(yyvsp[-4].ptr), (yyvsp[-2].ptr));
					  Action = "";
					  pull = NULL;
					}
#line 2208 "gram.c" /* yacc.c:1646  */
    break;

  case 140:
#line 497 "gram.y" /* yacc.c:1646  */
    { (yyval.num) = (yyvsp[0].num); }
#line 2214 "gram.c" /* yacc.c:1646  */
    break;

  case 141:
#line 498 "gram.y" /* yacc.c:1646  */
    {
				(yyval.num) = (yyvsp[-1].num);
				Action = (yyvsp[0].ptr);
				switch ((yyvsp[-1].num)) {
				  case F_MENU:
				    pull = GetRoot ((yyvsp[0].ptr), NULLSTR,NULLSTR);
				    pull->prev = root;
				    break;
				  case F_WARPRING:
				    if (!CheckWarpRingArg (Action)) {
					twmrc_error_prefix();
					fprintf (stderr,
			"ignoring invalid f.warptoring argument \"%s\"\n",
						 Action);
					(yyval.num) = F_NOP;
				    }
				  case F_WARPTOSCREEN:
				    if (!CheckWarpScreenArg (Action)) {
					twmrc_error_prefix();
					fprintf (stderr,
			"ignoring invalid f.warptoscreen argument \"%s\"\n",
					         Action);
					(yyval.num) = F_NOP;
				    }
				    break;
				  case F_COLORMAP:
				    if (CheckColormapArg (Action)) {
					(yyval.num) = F_COLORMAP;
				    } else {
					twmrc_error_prefix();
					fprintf (stderr,
			"ignoring invalid f.colormap argument \"%s\"\n",
						 Action);
					(yyval.num) = F_NOP;
				    }
				    break;
				} /* end switch */
				   }
#line 2257 "gram.c" /* yacc.c:1646  */
    break;

  case 142:
#line 544 "gram.y" /* yacc.c:1646  */
    { (yyval.num) = (yyvsp[0].num);
					  if ((yyvsp[0].num) == 0)
						yyerror("bad button 0");

					  if ((yyvsp[0].num) > MAX_BUTTONS)
					  {
						(yyval.num) = 0;
						yyerror("button number too large");
					  }
					}
#line 2272 "gram.c" /* yacc.c:1646  */
    break;

  case 143:
#line 556 "gram.y" /* yacc.c:1646  */
    { ptr = strdup((yyvsp[0].ptr));
					  RemoveDQuote(ptr);
					  (yyval.ptr) = ptr;
					}
#line 2281 "gram.c" /* yacc.c:1646  */
    break;

  case 144:
#line 561 "gram.y" /* yacc.c:1646  */
    { (yyval.num) = (yyvsp[0].num); }
#line 2287 "gram.c" /* yacc.c:1646  */
    break;


#line 2291 "gram.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 564 "gram.y" /* yacc.c:1906  */

static void
yyerror(const char *s)
{
    twmrc_error_prefix();
    fprintf (stderr, "error in input file:  %s\n", s ? s : "");
    ParseError = 1;
}

static void
RemoveDQuote(char *str)
{
    register char *i, *o;
    register int n;
    register int count;

    for (i=str+1, o=str; *i && *i != '\"'; o++)
    {
	if (*i == '\\')
	{
	    switch (*++i)
	    {
	    case 'n':
		*o = '\n';
		i++;
		break;
	    case 'b':
		*o = '\b';
		i++;
		break;
	    case 'r':
		*o = '\r';
		i++;
		break;
	    case 't':
		*o = '\t';
		i++;
		break;
	    case 'f':
		*o = '\f';
		i++;
		break;
	    case '0':
		if (*++i == 'x')
		    goto hex;
		else
		    --i;
	    case '1': case '2': case '3':
	    case '4': case '5': case '6': case '7':
		n = 0;
		count = 0;
		while (*i >= '0' && *i <= '7' && count < 3)
		{
		    n = (n<<3) + (*i++ - '0');
		    count++;
		}
		*o = n;
		break;
	    hex:
	    case 'x':
		n = 0;
		count = 0;
		while (i++, count++ < 2)
		{
		    if (*i >= '0' && *i <= '9')
			n = (n<<4) + (*i - '0');
		    else if (*i >= 'a' && *i <= 'f')
			n = (n<<4) + (*i - 'a') + 10;
		    else if (*i >= 'A' && *i <= 'F')
			n = (n<<4) + (*i - 'A') + 10;
		    else
			break;
		}
		*o = n;
		break;
	    case '\n':
		i++;	/* punt */
		o--;	/* to account for o++ at end of loop */
		break;
	    case '\"':
	    case '\'':
	    case '\\':
	    default:
		*o = *i++;
		break;
	    }
	}
	else
	    *o = *i++;
    }
    *o = '\0';
}

static MenuRoot *GetRoot(const char *name, const char* fore, const char *back)
{
    MenuRoot *tmp;

    tmp = FindMenuRoot(name);
    if (tmp == NULL)
	tmp = NewMenuRoot(name);

    if (fore)
    {
	int save;

	save = Scr->FirstTime;
	Scr->FirstTime = TRUE;
	GetColor(COLOR, &tmp->hi_fore, fore);
	GetColor(COLOR, &tmp->hi_back, back);
	Scr->FirstTime = save;
    }

    return tmp;
}

static void GotButton(int butt, int func)
{
    int i;

    for (i = 0; i < NUM_CONTEXTS; i++)
    {
	if ((cont & (1 << i)) == 0)
	    continue;

	Scr->Mouse[butt][i][mods].func = func;
	if (func == F_MENU)
	{
	    pull->prev = NULL;
	    Scr->Mouse[butt][i][mods].menu = pull;
	}
	else
	{
	    root = GetRoot(TWM_ROOT, NULLSTR, NULLSTR);
	    Scr->Mouse[butt][i][mods].item = AddToMenu(root,"x",Action,
		    NULL, func, NULLSTR, NULLSTR);
	}
    }
    Action = "";
    pull = NULL;
    cont = 0;
    mods_used |= mods;
    mods = 0;
}

static void GotKey(char *key, int func)
{
    int i;

    for (i = 0; i < NUM_CONTEXTS; i++)
    {
	if ((cont & (1 << i)) == 0)
	  continue;
	if (!AddFuncKey(key, i, mods, func, Name, Action))
	  break;
    }

    Action = "";
    pull = NULL;
    cont = 0;
    mods_used |= mods;
    mods = 0;
}


static void GotTitleButton (char *bitmapname, int func, Bool rightside)
{
    if (!CreateTitleButton (bitmapname, func, Action, pull, rightside, True)) {
	twmrc_error_prefix();
	fprintf (stderr,
		 "unable to create %s titlebutton \"%s\"\n",
		 rightside ? "right" : "left", bitmapname);
    }
    Action = "";
    pull = NULL;
}

static Bool CheckWarpScreenArg (char *s)
{
    XmuCopyISOLatin1Lowered (s, s);

    if (strcmp (s,  WARPSCREEN_NEXT) == 0 ||
	strcmp (s,  WARPSCREEN_PREV) == 0 ||
	strcmp (s,  WARPSCREEN_BACK) == 0)
      return True;

    for (; *s && isascii(*s) && isdigit(*s); s++) ; /* SUPPRESS 530 */
    return (*s ? False : True);
}


static Bool CheckWarpRingArg (char *s)
{
    XmuCopyISOLatin1Lowered (s, s);

    if (strcmp (s,  WARPSCREEN_NEXT) == 0 ||
	strcmp (s,  WARPSCREEN_PREV) == 0)
      return True;

    return False;
}


static Bool CheckColormapArg (char *s)
{
    XmuCopyISOLatin1Lowered (s, s);

    if (strcmp (s, COLORMAP_NEXT) == 0 ||
	strcmp (s, COLORMAP_PREV) == 0 ||
	strcmp (s, COLORMAP_DEFAULT) == 0)
      return True;

    return False;
}


void
twmrc_error_prefix (void)
{
    fprintf (stderr, "%s:  line %d:  ", ProgramName, yylineno);
}
